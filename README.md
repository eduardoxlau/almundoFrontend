# AlmundoFrontend
this project was created in angular 4.4  and Angular CLI 1.2.0 using concepts of responsive desing, it uses a backend made with nodejs, mongodb and elasticsearch,you can download backend here *https://gitlab.com/eduardoxlau/almundo, for visit deployed project here *http://104.196.97.170

## install package
run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
