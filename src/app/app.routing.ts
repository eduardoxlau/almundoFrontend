import {Routes, RouterModule} from "@angular/router";
import {FilterComponent} from "./card-hotel/filter.component";
import {CardHotelComponent} from "./card-hotel/card-hotel.component";
const APP_ROUTES: Routes=[
  { path: 'home',component: CardHotelComponent},
{ path: 'filter/:search/:filter',component: FilterComponent},
  //{ path: 'dashboard',component: DashboardComponent, canActivate: [AuthGuard]},
  //{ path: 'administrators',component: AdministratorComponent},
  //{ path: 'pacient',component: PacientComponent,children: PACIENT_ROUTES},
  //{ path: 'doctor',component: DoctorComponent,children: DOCTOR_ROUTES},
  //{  path: 'profile',component: ProfileAdministratorComponent},
  //{  path: 'tips',component: TipsComponent,children: TIPS_ROUTES},

  {  path: '',redirectTo: "/home",pathMatch:"full"},


];
export const routing=RouterModule.forRoot(APP_ROUTES);