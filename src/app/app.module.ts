import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {routing} from "./app.routing";
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { VerticalMenuComponent } from './vertical-menu/vertical-menu.component';
import { StarsComponent } from './stars/stars.component';
import { CardHotelComponent } from './card-hotel/card-hotel.component';
import { FilterComponent } from './card-hotel/filter.component';
import { MenuMovilComponent } from './menu-movil/menu-movil.component';
import { HotelService } from './hotel.service';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    VerticalMenuComponent,
    StarsComponent,
    CardHotelComponent,
    MenuMovilComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},HotelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
