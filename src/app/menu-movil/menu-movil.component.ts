import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-movil',
  templateUrl: './menu-movil.component.html',
  styleUrls: ['./menu-movil.component.css']
})
export class MenuMovilComponent implements OnInit {
  name:boolean=true;
  stars:boolean=true;
  starOptions:any=[];
  value="";
  items;
  check:any={all:false,one:false,two:false,three:false,four:false,five:false};
  @Output() search: EventEmitter<any> = new EventEmitter();
  //startsMarker:any=[{one:false,two:false;three:false,four:false,five:false,all:false}];
  constructor() { }

  ngOnInit() {
  }
  menu(){
    this.items=!this.items;
  }
  

   onSubmit(data:any){
     console.log(data);
     this.value=null;
     this.menu();
     this.search.emit({search:data.value,filter:this.starOptions});
     this.check={all:false,one:false,two:false,three:false,four:false,five:false};

   }
  logCheckbox(star:number,element: HTMLInputElement): void {
    if(element.checked){
      this.starOptions.push(star);
    }else{
      let index=this.starOptions.indexOf(star);
      if (index > -1) {
        this.starOptions.splice(index, 1);
      }
    }
    this.search.emit({search:null,filter:this.starOptions});
    console.log(this.starOptions);
    this.menu();
  }

}
