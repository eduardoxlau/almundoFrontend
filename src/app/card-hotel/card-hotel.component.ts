import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
@Component({
  selector: 'app-card-hotel',
  templateUrl: './card-hotel.component.html',
  styleUrls: ['./card-hotel.component.css']
})
export class CardHotelComponent implements OnInit {
	datahotels:any;
  load:boolean=false;

  constructor(private hotels: HotelService) { }

  ngOnInit() {
    this.load=true;
  	this.hotels.hotels()
      .subscribe(
        (data)=>{
          this.load=false
          ;
         console.log(data);
         this.datahotels=data.results;
         for(let h of this.datahotels){
         		h.stars=Array(h.stars).fill(1);
             console.log(h.image.substring(0, 4));
             if(h.image.substring(0, 4)!="http"){
               h.image="assets/images/hotels/"+h.image;
             }
            
         }
        }
    );
  }
}
