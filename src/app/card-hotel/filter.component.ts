import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-card-hotel',
  templateUrl: './card-hotel.component.html',
  styleUrls: ['./card-hotel.component.css']
})
export class FilterComponent implements OnInit {
	datahotels:any;
	load:boolean=false;
  private  subscription:Subscription;
  constructor(private hotels: HotelService,private route:ActivatedRoute) { }

  ngOnInit() {
  	this.subscription =this.route.params.subscribe(
      (params: any)=>{
        
        	var array = params['filter'].split(',');
        	console.log(array);
        	this.filterstars({data:array,search:params['search']});

        console.log(params['search']);
      }

    );
  	/*this.hotels.hotels()
      .subscribe(
        (data)=>{
         console.log(data);
         this.datahotels=data.results;
         for(let h of this.datahotels){
         		h.stars=Array(h.stars).fill(1);
         }
        }
    );*/
  }
  filterstars(data){
  	this.load=true;
  	this.hotels.filter(data)
      .subscribe(
        (data)=>{
         console.log(data);
         this.load=false;
         this.datahotels=data.results;
         for(let h of this.datahotels){
         		h.stars=Array(h.stars).fill(1);
         		console.log(h.image.substring(0, 4));
         		if(h.image.substring(0, 4)!="http"){
	               h.image="assets/images/hotels/"+h.image;
	             }
         }
        }
    );
  }
}
