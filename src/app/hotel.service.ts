import { Injectable } from '@angular/core';
import {Response, Http, Headers} from "@angular/http";
import 'rxjs/Rx';
@Injectable()
export class HotelService {
  domain:string = "http://104.196.97.170";
  //domain:string = "http://localhost:3000";
  constructor(private http: Http) { 

  }
  hotels(){
     return this.http.get(`${this.domain}/api/hotels`)
     .map((response: Response) => response.json());
  }
  filter(data){
     return this.http.post(`${this.domain}/api/filter`,data)
     .map((response: Response) => response.json());
  }
  // loterias(){
  //    return this.http.get(`${this.domain}/resultados/index.php/loterias`)
  //    .map((response: Response) => response.json());
  // }
  // loterias_last(){
  //    return this.http.get(`${this.domain}/resultados/index.php/loterias_last`)
  //    .map((response: Response) => response.json());
  // }
}
