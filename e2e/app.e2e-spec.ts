import { AlmundoFrontendPage } from './app.po';

describe('almundo-frontend App', () => {
  let page: AlmundoFrontendPage;

  beforeEach(() => {
    page = new AlmundoFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
